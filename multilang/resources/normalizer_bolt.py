import storm
import re

class NormalizerBolt(storm.BasicBolt):

    def initialize(self, conf, context):
        self._conf = conf
        self._context = context

        self._common_words = [
            "the", "be", "a", "an", "and", "of", "to", "in", "am", "is", "are",
            "at", "not", "that", "have", "i", "it", "for", "on", "with", "he",
            "she", "as", "you", "do", "this", "but", "his", "by", "from",
            "they", "we", "her", "or", "will", "my", "one", "all", "s", "if",
            "any", "our", "may", "your", "these", "d", " ", "me", "so", "what",
            "him", "their"
        ]

        storm.logInfo("Normalizer bolt instance starting...")

    def process(self, tup):
        # TODO:
        # Task 1: make the words all lower case
        # Task 2: remove the common words
        words = re.split(r'[^a-zA-Z0-9-]', tup.values[0])
        #filtered_words = [word.lower() for word in words if word not in self._common_words]
        filtered_words = [word.strip().lower() for word in words if word.strip().lower() not in self._common_words]
        for word in filtered_words:
            storm.logInfo("SplitEmitting %s" % word)
            storm.emit([word])
        pass
        # End


# Start the bolt when it's invoked
NormalizerBolt().run()
