# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

---

# topology definition
# name to be used when submitting
name: "part-D"

# TODO
# Task: implement your topology for part d

# topology configuration
# this will be passed to the submitter as a map of config options
config:
    storm.local.hostname: "localhost"
    topology.max.task.parallelism: 3
    # set this to true if you want to see more debug log
    # set it to false before submitting
    topology.debug: false
    # Hint: you can set input file path here
    # make sure it's "/tmp/data.txt" in your final submission
    topology.local.dir: "/tmp/bible.txt"
    # Hint: set N here
    # make sure it's 10 in your final submission
    topology.local.topN: 10

components:
  - id: "poolConfig"
    className: "org.apache.storm.redis.common.config.JedisPoolConfig"
    constructorArgs:
      - "localhost"
      - 6379
      - 2000
      - "uiuc_cs498_mp6"
      - 0

  # TODO
  # Task: implement the redis store mapper
  # src/main/java/edu/illinois/storm/TopNStoreMapper.java
  # redis hash key for part D is "partDTopN"
  - id: "storeMapper"
    className: "edu.illinois.storm.TopNStoreMapper"
    constructorArgs:
      - "partDTopN"

# spout definitions
spouts:
  - id: "sentence-spout"
    className: "org.apache.storm.flux.wrappers.spouts.FluxShellSpout"
    constructorArgs:
      # Command line
      - ["python", "file_reader_spout.py"]
      # Output field(s)
      - ["sentence"]
    # parallelism hint
    parallelism: 1

# bolt definitions
bolts:
  # Hint: the new top N tracking bolt you need implement in this part is
  # multilang/resources/top_n_finder_bolt.py
  # You need to load N from conf when initialize the bolt
  - id: "splitter-bolt"
    className: "org.apache.storm.flux.wrappers.bolts.FluxShellBolt"
    constructorArgs:
      # Command line
      - ["python", "split_sentence_bolt.py"]
      # Output field(s)
      - ["word"]
    parallelism: 1

  - id: "normalizer-splitter-bolt"
    className: "org.apache.storm.flux.wrappers.bolts.FluxShellBolt"
    constructorArgs:
      # Command line
      - ["python", "normalizer_bolt.py"]
      # Output field(s)
      - ["word"]
    parallelism: 1

  - id: "counter-bolt"
    className: "org.apache.storm.flux.wrappers.bolts.FluxShellBolt"
    constructorArgs:
      # Command line
      - ["python", "word_count_bolt.py"]
      # Output field(s)
      - ["word","count"]
    parallelism: 1

  - id: "topNcounter-bolt"
    className: "org.apache.storm.flux.wrappers.bolts.FluxShellBolt"
    constructorArgs:
      # Command line
      - ["python", "top_n_word_count_bolt.py"]
      # Output field(s)
      - ["word","count"]
    parallelism: 1

  - id: "top_n_finder-bolt"
    className: "org.apache.storm.flux.wrappers.bolts.FluxShellBolt"
    constructorArgs:
      # Command line
      - ["python", "top_n_finder_bolt.py"]
      # Output field(s)
      - ["top-N","top-N-str"]
    parallelism: 1

  - id: "redis-bolt"
    className: "org.apache.storm.redis.bolt.RedisStoreBolt"
    constructorArgs:
      # Command line
      - ref: "poolConfig"
      - ref: "storeMapper"
    parallelism: 1

      # Logging
  - id: "log"
    className: "org.apache.storm.flux.wrappers.bolts.LogInfoBolt"
    parallelism: 1


# stream definitions
# stream definitions define connections between spouts and bolts.
streams:
  # Hint: add new top N finder bolt into the topology
  - name: "Spout --> normalizer-splitter-bolt" # name isn't used (placeholder for logging, UI, etc.)
    # The stream emitter
    from: "sentence-spout"
    # The stream consumer
    to: "normalizer-splitter-bolt"
    # Grouping type
    grouping:
      type: SHUFFLE

  # TODO
  # Task: pipe output of split bolt to word count bolt
  # Hint: choose the right grouping type to make problem easier
  - name: "normalizer-splitter-bolt -> topNcounter"
    from: "normalizer-splitter-bolt"
    to: "topNcounter-bolt"
    grouping:
      type: FIELDS
      # field(s) to group on
      args: ["word"]

  - name: "topNcounter -> TopN"
    from: "topNcounter-bolt"
    to: "top_n_finder-bolt"
    grouping:
      type: FIELDS
      # field(s) to group on
      args: ["word", "count"]

  - name: "top_n_finder-bolt -> redis-bolt"
    from: "top_n_finder-bolt"
    to: "redis-bolt"
    grouping:
      type: SHUFFLE