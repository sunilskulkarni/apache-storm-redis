import heapq
from collections import Counter
from collections import defaultdict
from heapq import heapify, heappush, heappushpop
import storm
import datetime
import json

class TopNFinderBolt(storm.BasicBolt):
    # Initialize this instance
    def initialize(self, conf, context):
        self._conf = conf
        self._context = context

        #storm.logInfo("Top N Counter bolt instance starting...")
        now = datetime.datetime.now()
        storm.logInfo("Top N Counter bolt instance starting...:%s,%s" % (now.minute, now.second))
        # TODO:
        # Task: set N
        self.N = self._conf['topology.local.topN']
        #storm.logInfo("Top N:%s" % self.N)
        # self.heap = []
        # heapq.heapify(heap)
        # End

        # Hint: Add necessary instance variables and classes if needed
        self.word_dic = defaultdict(int)
        self.heap = []
        heapq.heapify(self.heap)
        self.max_n = int(self.N)
        pass


    def process(self, tup):
        #https://stackoverflow.com/questions/30443150/maintain-a-fixed-size-heap-python
        #https://stackoverflow.com/questions/3954530/how-to-make-heapq-evaluate-the-heap-off-of-a-specific-attribute

        #https://towardsdatascience.com/data-structure-heap-23d4c78a6962
        '''
        TODO:
        Task: keep track of the top N words
        Hint: implement efficient algorithm so that it won't be shutdown before task finished
              the algorithm we used when we developed the auto-grader is maintaining a N size min-heap
        '''
        #storm.logInfo("TOPN nextTuple called...")
        # word1 = tup.values[0]
        # count1 = tup.values[1]
        # storm.logInfo("TOPN: finder tuple: %s:%s" % (word1, str(count1)))

        word, inc = tup.values

        #--------------------------------
        # if word and inc and word != 'eof':
        #
        #     if len(self.heap) < self.max_n:
        #         heapq.heappush(self.heap, (inc, word))
        #     else:
        #         heapq.heappushpop(self.heap, (inc, word))
        #--------------------------------


        if not (word and inc):
            return list(self.heap)
        self.word_dic[word] += inc
        count = self.word_dic[word]
        words = dict([(k, v) for (v, k) in self.heap])
        if word in words:
            words.update({word: count})
            self.heap = list((v, k) for (k, v) in words.items())
            heapq.heapify(self.heap)
        elif len(self.heap) < self.max_n:
            heapq.heappush(self.heap, (count, word))
        else:
            heapq.heappushpop(self.heap, (count, word))


        #storm.logInfo("TOP-N:%s" % ', '.join([k for (v, k) in self.heap]))
        #str1 = ', '.join("%s" % (key) for (key,val) in self.word_dic.items())
        if (word == 'eof'):
            #storm.emit(["top-N", ', '.join([k for (v, k) in self.heap])])
            storm.emit(["top-N", json.dumps(self.heap)])
            now1 = datetime.datetime.now()
            storm.logInfo("TOPN Emit to Redis...:%s,%s" % (now1.minute, now1.second))
        #storm.logInfo("TOPN Emit to Redis...:%s" % datetime.datetime.now())
        #keys
        #storm.emit(["top-N", ', '.join([k for (v, k) in self.heap])])


        pass
        # End


# Start the bolt when it's invoked
TopNFinderBolt().run()
